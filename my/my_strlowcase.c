/*
** my_strlowcase.c for my_strlowcase in /home/abet_v//afs/Jour_06/ex_08
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Mon Oct  8 16:43:09 2012 vincent abet
** Last update Tue Oct  9 10:12:23 2012 vincent abet
*/

char	*my_strlowcase(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] >= 'A' && str[i] <= 'Z')
	{
	  str[i] = str[i] + 32;
	}
      i = i + 1;
    }
  return (str);
}
