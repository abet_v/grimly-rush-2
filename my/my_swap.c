/*
** my_swap.c for my_swap in /home/abet_v//afs/Jour_04
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Thu Oct  4 09:49:03 2012 vincent abet
** Last update Tue Oct  9 10:25:14 2012 vincent abet
*/

void	my_swap(int *a, int *b)
{
  int	temp;

  temp = *a;
  *a = *b;
  *b = temp;
}
