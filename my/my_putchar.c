/*
** my_putchar.c for my_putchar in /home/abet_v//afs/lib
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Tue Oct  9 09:42:49 2012 vincent abet
** Last update Tue Oct  9 09:43:27 2012 vincent abet
*/

void	my_putchar(char c)
{
  write(1, &c, 1);
}
