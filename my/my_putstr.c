/*
** my_putstr.c for my_putstr in /home/abet_v//afs/lib
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Tue Oct  9 09:45:10 2012 vincent abet
** Last update Tue Oct  9 10:40:28 2012 vincent abet
*/

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i]);
      i = i + 1;
    }
}
