/*
** convert_base.c for convert_base in /home/abet_v//afs/Jour_08/ex_02
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Wed Oct 10 09:41:22 2012 vincent abet
** Last update Thu Oct 11 18:55:17 2012 vincent abet
*/

#include <stdlib.h>

char	*convert_base(char *nbr, char *base_from, char *base_to)
{
  int	i;
  int	n;
  int	base_from_l;
  int	base_to_l;
  int	result;
  char	*result_base_to;
  int	leng;

  base_to_l = my_strlen(base_to);
  result = convert_base_dix(nbr, base_from);
  leng = sizemalloc(result, base_to_l);
  result_base_to = malloc(leng);
  i = 0;
  while (result > 0)
    {
      result_base_to[i] = base_to[result % base_to_l];
      result = result / base_to_l;
      i = i + 1;
    }
  result_base_to[i] = '\0';
  my_revstr(result_base_to);
  return (result_base_to);
}

int	sizemalloc(int	result, int base_to_l)
{
  int	i;

  while (result > 0)
    {
      result = result / base_to_l;
      i = i + 1;
    }
  i = i + 1;
  return (i);
}

int	convert_base_dix(char *nbr, char *base_from)
{
  int	i;
  int	n;
  int	base_from_l;
  int	result;

  base_from_l = my_strlen(base_from);
  i = 0;
  result = 0;
  while (nbr[i] != 0)
    {
      n = 0;
      while (nbr[i] != base_from[n])
	{
	  n = n + 1;
	}
      if (nbr[i + 1] != 0)
	{
	  result = (result + n) * base_from_l;
	}
      i = i + 1;
    }
  result = result + n;
  return (result);
}
