/*
** my_strlen.c for my_strlen in /home/abet_v//afs/Jour_04
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Thu Oct  4 10:03:36 2012 vincent abet
** Last update Thu Oct  4 14:30:58 2012 vincent abet
*/

int my_strlen(char *str)
{
  int	leng;

  leng = 0;
  while (*str != '\0')
    {
      leng = leng + 1;
      str = str + 1;
    }
  return (leng);
}
