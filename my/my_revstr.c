/*
** my_revstr.c for my_revstr in /home/abet_v//afs/Jour_06/ex_03
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Mon Oct  8 11:58:10 2012 vincent abet
** Last update Mon Oct  8 22:30:25 2012 vincent abet
*/

char	*my_revstr(char *str)
{
  int	leng;
  char	*a;
  char	*b;
  char	c;

  a = str;
  leng = my_strlen(str);
  b = str + leng - 1;
  while (a < b)
    {
      c = *b;
      *b = *a;
      *a = c;
      a = a + 1;
      b = b - 1;
    }
  return (str);
}


int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}
