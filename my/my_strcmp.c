/*
** my_strcmp.c for my_strcmp in /home/abet_v//afs/Jour_06/ex_05
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Mon Oct  8 16:43:09 2012 vincent abet
** Last update Mon Oct  8 22:35:18 2012 vincent abet
*/

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] == s2[i] && s1[i] != '\0')
    {
      i = i + 1;
    }
  if (s1[i] > s2[i])
    {
      return (1);
    }
  if (s1[i] < s2[i])
    {
      return (-1);
    }
  else
    {
  return (0);
    }
}
