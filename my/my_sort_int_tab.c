/*
** my_sort_int_tab.c for my_sort_int_tab in /home/abet_v//afs/Jour_04
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Tue Oct  9 11:35:49 2012 vincent abet
** Last update Tue Oct  9 13:06:43 2012 vincent abet
*/

void	my_sort_int_tab(char *tab, int size)
{
  int	i;
  int	swap;

  i = 0;
  while (i <= size)
    {
      if (tab[i] > tab[i + 1] && tab[i + 1] != '\0')
	{
	  swap = tab[i + 1];
	  tab[i + 1] = tab[i];
	  tab[i] = swap;
	  i = i - 2;
	}
      i = i + 1;
    }
  i = 0;
  while (i <= size)
    {
      my_putchar(tab[i]);
      i = i + 1;
    }
}
