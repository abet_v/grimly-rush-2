/*
** my_strupcase.c for my_strupcase in /home/abet_v//afs/Jour_06/ex_05
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Mon Oct  8 16:43:09 2012 vincent abet
** Last update Tue Oct  9 10:11:36 2012 vincent abet
*/

char	*my_strupcase(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] >= 'a' && str[i] <= 'z')
	{
	  str[i] = str[i] - 32;
	}
      i = i + 1;
    }
  return (str);
}
