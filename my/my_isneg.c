/*
** my_isneg.c for my_isneg in /home/abet_v//afs/Jour_03
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Wed Oct  3 11:39:49 2012 vincent abet
** Last update Thu Oct  4 14:51:50 2012 vincent abet
*/

int	my_isneg(int n)
{
  if (n >= 0)
    {
      my_putchar('P');
    }
  else
    {
      my_putchar('N');
    }
}
