/*
** my_square_root.c for my_square_root in /home/abet_v//afs/Jour_05
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Tue Oct  9 13:11:07 2012 vincent abet
** Last update Tue Oct  9 14:26:15 2012 vincent abet
*/

int	my_square_root(int nb)
{
  int	s;
  int	r;

  s = 0;
  while (s <= nb)
    {
      r = s * s;
      if (r == nb)
	{
	  return (s);
	}
      s = s + 1;
    }
  return (0);
}
