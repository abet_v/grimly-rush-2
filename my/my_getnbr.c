/*
** my_getnbr.c for libmy in /home/rahm_j//afs/lib
** 
** Made by jeremie rahm
** Login   <rahm_j@epitech.net>
** 
** Started on  Fri Oct 12 18:22:16 2012 jeremie rahm
** Last update Mon Oct 22 15:03:56 2012 vincent abet
*/

int	my_getnbr(char *str)
{
  int	lenght;
  int	result;
  int	sign;
  int	numbers;

  numbers = 0;
  result = 0;
  sign = 1;
  verif_char(str);
  lenght = my_strlen(str);
  while (0 < lenght)
    {
      if (*str == '-')
	  sign = sign * (-1);
      if (*str != '+' && *str >= 48 && *str <= 57)
	{
	  numbers = numbers + 1;
	  result = result + (*str - 48) * power(lenght - 1);
	  if (numbers == 10 && result < 0 || numbers > 10)
	      return (0);
	}
      lenght = lenght - 1;
      str = str + 1;
    }
  return (result * sign);
}

int	verif_char (char *str)
{
  int	start;

  start = 0;
  while (*str != '\0')
    {
      if ((48 > *str || *str > 57) && (*str != 43 && *str != 45))
	{
	  *str = '\0';
	  return (0);
	}
      if ((*str == 43 && start == 1) || (*str == 45 && start == 1))
	{
	  *str = '\0';
	  return (0);
	}
      if (48 < *str && *str < 57)
	{
	  start = 1;
	}
      str = str + 1;
    }
}
