/*
** my_put_nbr.c for my_put_nbr in /home/abet_v//afs/Jour_03
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Wed Oct  3 17:04:19 2012 vincent abet
** Last update Wed Nov 14 13:45:32 2012 vincent abet
*/

#define VAL_IN_BASE(x) (base[x])

void	my_put_nbr_base(int nb, char *base)
{
  int	leng;
  int	displaynbr;
  int	base_len;


  base_len = my_strlen(base);
  if (nb < 0)
    {
      nb = nb * (-1);
      my_putchar('-');
    }
  leng = lenght_ba(nb, base_len);
  while (leng >= 0)
    {
      displaynbr = nb / my_power_rec(base_len, leng);
      my_putchar(VAL_IN_BASE(displaynbr));
      nb = nb - my_power_rec(base_len, leng) * displaynbr;
      leng = leng - 1;
    }
}

int     lenght_ba(int nb, int b)
{
  int   leng;

  leng = -1;
  while (nb > 0)
    {
      nb = nb / b;
      leng = leng + 1;
    }
  return (leng);
}
