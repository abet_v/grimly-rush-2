/*
** my_put_nbr.c for my_put_nbr in /home/abet_v//afs/Jour_03
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Wed Oct  3 17:04:19 2012 vincent abet
** Last update Mon Oct 22 15:26:03 2012 vincent abet
*/

void	my_put_nbr(int nb)
{
  int	leng;
  int	displaynbr;

  if (nb < 0)
    {
      nb = nb * (-1);
      my_putchar('-');
    }
  if (nb == 0)
    my_putchar('0');
  leng = lenght(nb);
  while (leng >= 0)
    {
      displaynbr = nb / power(leng);
      my_putchar(displaynbr + '0');
      nb = nb - power(leng) * displaynbr;
      leng = leng - 1;
    }
}

int	lenght(int nb)
{
  int	leng;
  
  leng = -1;
  while (nb > 0)
    {
      nb = nb / 10;
      leng = leng + 1;
    }
  return (leng);
}

int	power(int powr)
{
  int	up;
  
  up = 1;
  while (powr > 0)
    {
      up = up * 10;
      powr = powr - 1;
    }
  return (up);
}
