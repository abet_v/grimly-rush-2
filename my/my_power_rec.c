/*
** my_power_rec.c for my_power_rec in /home/abet_v//afs/Jour_05
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Fri Oct  5 17:11:04 2012 vincent abet
** Last update Sun Oct  7 17:45:17 2012 vincent abet
*/

int	my_power_rec(int nb, int power)
{
  int	a;

  a = nb;
  if (power < 1)
    {
      return (1);
    }
  nb = a * my_power_rec(nb, power - 1);
  return (nb);
}
