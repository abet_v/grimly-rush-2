/*
** my_find_prime_sup.c for my_find_prime_sup in /home/abet_v//afs/lib/all
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Sun Oct 21 23:00:53 2012 vincent abet
** Last update Mon Oct 22 09:34:23 2012 vincent abet
*/

int	my_find_prime_sup(int nb)
{
  int	sup;

  while ((sup = my_is_prime(nb)) == 0)
    {
      nb = nb + 1;
    }
  return (nb);
}
