/*
** my_strcat.c for my_strcat in /home/abet_v//afs/Jour_07
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Tue Oct  9 14:44:34 2012 vincent abet
** Last update Thu Dec 13 18:20:16 2012 vincent abet
*/

#include <stdlib.h>

char	*my_strcat(char *dest, char *src)
{
  int	i;
  int	leng;
  char	*res;

  i = 0;
  res = malloc((my_strlen(dest) + my_strlen(src)) * sizeof(res));
  while (dest[i])
    {
      res[i] = dest[i];
      i++;
    }
  leng = i;
  i = 0;
  while (src[i] != '\0')
    {
      res[leng + i] = src[i];
      i = i + 1;
    }
  res[i + leng] = 0;
  return (res);
}
