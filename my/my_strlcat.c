/*
** my_strlcat.c for my_strlcat in /home/abet_v//afs/lib
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Tue Oct  9 15:56:24 2012 vincent abet
** Last update Tue Oct  9 18:12:53 2012 vincent abet
*/

int	my_strlcat(char *dest, char *src, int size)
{
  int	i;
  int	leng;

  i = 0;
  leng = my_strlen(dest);
  while (i <= size - leng - 1 && src[i] != '\0')
    { 
      if (i > size)
	{
	  return (i);
	}
      dest[leng + i] = src[i];
      i = i + 1;
    }
  dest[i + leng] = '\0';
  return (i);
}
