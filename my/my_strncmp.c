/*
** my_strcmp.c for my_strcmp in /home/abet_v//afs/Jour_06/ex_05
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Mon Oct  8 16:43:09 2012 vincent abet
** Last update Thu Dec 13 15:47:53 2012 vincent abet
*/

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;
  int	check;

  check = 0;
  i = 0;
  while ((s1[i] == s2[i] || s1[i] != '\0') && i < n)
    {
      if (s1[i] > s2[i])
	{
	  return (1);
	}
      if (s1[i] < s2[i])
	{
	  return (-1);
	}
      i = i + 1;
    }
  if (i != n)
    return (1);
  return (0);
}
