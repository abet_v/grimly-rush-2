/*
** my_is_prime.c for my_is_prime in /home/abet_v//afs/lib/all
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Sun Oct 21 22:56:26 2012 vincent abet
** Last update Mon Oct 22 09:29:32 2012 vincent abet
*/

int	my_is_prime(int nb)
{
  int	i;

  i = 2;
  if (nb == 0 || nb == 1)
    {
      return (0);
    }
  while (i < nb)
    {  
      if (nb % i == 0)
	{
	  return (0);
	}
      i = i + 1;
    }
  return (1);
}
