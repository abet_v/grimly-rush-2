/*
** my_strcapitalize.c for my_strcapitalize in /home/abet_v//afs/Jour_06/ex_09
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Mon Oct  8 16:43:09 2012 vincent abet
** Last update Tue Oct  9 10:13:25 2012 vincent abet
*/

char	*my_strcapitalize(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[0] >= 'a' && str[i] <= 'z')
	{
	  str[0] = str[0] - 32;
	}
      if (str[i] >= 'A' && str[i] <= 'Z')
	{
	  str[i] = str[i] + 32;
	}
      if (str[i] == ' ' || str[i] == ';' || str[i] == '+' || str[i] == '-')
	{
	  if (str[i + 1] >= 'a' && str[i] <= 'z')
	    {
	      str[i +1] = str[i + 1] - 32;
	    }
	}
      i = i + 1;
    }
  return (str);
}
