/*
** my_strstr.c for my_strstr in /home/abet_v//afs/Jour_06/ex_04
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Mon Oct  8 13:42:44 2012 vincent abet
** Last update Mon Oct  8 22:34:14 2012 vincent abet
*/

char	*my_strstr(char *str, char *to_find)
{
  int	i;
  int	l;
  int	a;
  int	check;

  l = my_strlen(to_find);
  i = 0;
  while (str[i] != '\0')
    {
      check = 0;
      a = 0;
      while (a < l)
	{
	  if (str[i + a] == to_find[a])
	    {
	      check = check + 1;
	    }
	  a = a + 1;
	}
      if (check == l)
	  return (&str[i]);
      i = i + 1;
    }
  return (0);
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}
