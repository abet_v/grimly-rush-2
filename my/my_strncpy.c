/*
** my_strncpy.c for my_strncpy in /home/abet_v//afs/Jour_06/ex_02
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Mon Oct  8 10:32:04 2012 vincent abet
** Last update Mon Oct  8 22:29:09 2012 vincent abet
*/

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;
  int	stop;

  stop = 0;
  i = 0;
  while (i < n)
    {
      if (src != '\0')
	{
	  dest[i] = src[i];
	  stop = stop + 1;
	}
      i = i + 1;
    }
  dest[stop] = '\0';
  return (dest);
}
