/*
** my_strncat.c for my_strncat in /home/abet_v//afs/lib
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Tue Oct  9 15:28:12 2012 vincent abet
** Last update Tue Oct  9 18:14:18 2012 vincent abet
*/

char	*my_strncat(char *dest, char *src, int nb)
{
  int	i;
  int	leng;

  leng = my_strlen(dest);
  i = 0;
  while (i < nb && src[i] != '\0')
    {
      dest[i + leng] = src[i];
      i = i + 1;
    }
  dest[i + leng] = '\0';
  return (dest);
}
