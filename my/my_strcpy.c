/*
** my_strcpy.c for my_strcpy in /home/abet_v//afs/Jour_06/ex_01
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Mon Oct  8 09:36:28 2012 vincent abet
** Last update Mon Oct  8 10:31:26 2012 vincent abet
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;
  int	leng;

  leng = my_strlen(dest);
  i = 0;
  while (src[i] != '\0')
    {
      dest[i] = src[i];
      i = i + 1;
    }
  while (i < leng)
    {
      dest[i] = '\0';
      i = i + 1;
    }
  return (dest);
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      i = i + 1;
    }
   return (i);
}
