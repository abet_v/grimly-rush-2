/*
** my_str_isalpha.c for my_str_isalpha in /home/abet_v//afs/Jour_06/ex_09
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Mon Oct  8 16:43:09 2012 vincent abet
** Last update Mon Oct  8 22:47:57 2012 vincent abet
*/

int	my_str_isalpha(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] > 'z' || str[i] < 'A' || (str[i] >= 92 && str[i] <= 96))
	{
	  return (0);
	}
      i = i + 1;
    }
  return (1);
}
