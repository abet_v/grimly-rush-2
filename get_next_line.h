/*
** get_next_line.h for get_next_line.c in /home/abet_v//getnext
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Wed Dec  5 15:26:19 2012 vincent abet
** Last update Fri Mar  1 21:08:29 2013 vincent abet
*/

#define        BUF_SIZE 5000
#define        BUF_READ 5000

typedef struct    s_var
{
  char        *buffer;
  int        i;
  int        pos;
  int        nb_read;
}        t_var;

char        *get_next_line(const int fd);
