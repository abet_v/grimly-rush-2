/*
** main.c for grimly in /home/abet_v//rush
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Fri Mar  1 20:52:00 2013 vincent abet
** Last update Fri Mar  1 23:39:59 2013 vincent abet
*/

#include "grimly.h"

char    **my_put_tab(const int fd)
{
  char  **tab;
  char  *s;
  int   n;
  int   i;

  i = 0;
  s = get_next_line(fd);
  n = my_getnbr(s);
  free(s);
  tab = malloc((n + 1) * sizeof(*tab));
  if (tab == NULL)
    exit(my_putstr("could not alloc"));
  while ((s = get_next_line(fd)))
    {
      tab[i] = s;
      i++;
    }
  tab[i] = NULL;
  return (tab);
}

int	test_valid(char **tab)
{
  int	x;
  int	y;
  int	un;
  int	deux;

  un = 0;
  deux = 0;
  x = 0;
  while (tab[x])
    {
      y = 0;
      while (tab[x][y])
	{
	  if (tab[x][y] != ' ' && tab[x][y] != '1' && tab[x][y] != '2' && tab[x][y] != '*')
	    exit(my_putstr("MAP ERROR\n"));
	  if (tab[x][y] == '1')
	    un++;
	  if (tab[x][y] == '2')
	    deux++;
	  y++;
	}
      x++;
    }
  if (un != 1)
    exit(my_putstr("MAP ERROR : BUG ENTRY \n"));
  if (deux == 0)
    exit(my_putstr("MAP ERROR : NO EXIT \n"));
  return (0);
}

int	show_tab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i])
    {
      my_putstr(tab[i]);
      my_putchar('\n');
      i++;
    }
  return (0);
}

int	grimly(char *str)
{
  int	fd;
  char	**tab;

  if ((fd = open(str, O_RDONLY)) == -1)
    return (my_putstr("cannot open\n"));
  tab = my_put_tab(fd);
  test_valid(tab);
  show_tab(tab);
  tab = find_the_way(tab);
}

int	main(int ac, char **av)
{
  if (ac != 3)
    my_putstr("Usage : ./grimly -f mapname\n");
  else
    {
      if (av[1][0])
	if (av[1][1] == 'f')
	  grimly(av[2]);
    }
  return (0);
}
