/*
** grimly.h for grimly in /home/abet_v//rush
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Fri Mar  1 21:09:40 2013 vincent abet
** Last update Fri Mar  1 23:48:48 2013 vincent abet
*/


#ifndef	__GRIMLY_H__
#define	__GRIMLY_H__

#define	WALL	(0)
#define	ENTRY	(1)
#define	EXIT	(2)
#define	SPACE	(3)
#define	WAY	(4)

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include "my.h"
#include "get_next_line.h"

typedef struct	s_coord
{
  int	x;
  int	y;
}		t_coord;

typedef struct	s_lab
{
  t_coord	*prev;
  t_coord	curent;
}		t_lab;

char		**find_the_way(char **s);

#endif
