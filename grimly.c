/*
** grimly.c for grimly in /home/abet_v//rush
** 
** Made by vincent abet
** Login   <abet_v@epitech.net>
** 
** Started on  Fri Mar  1 22:33:30 2013 vincent abet
** Last update Fri Mar  1 23:49:41 2013 vincent abet
*/

#include "grimly.h"

int	my_tablen(char **tab)
{
  int	i;

  i = 0;
  while (tab[i])
    i++;
  return (i);
}

int	**mk_tabint(char **tab)
{
  int	**s;
  int	tablen;
  int	i;

  i = 0;
  tablen = my_tablen(tab);
  if ((s = malloc(tablen * sizeof(*s))) == NULL)
    exit(my_putstr("cannot alloc\n"));
  while (i < tablen)
    {
      if ((s[i] = malloc(my_strlen(tab[0]) * sizeof(s))) == NULL)
	exit(my_putstr("cannot alloc\n"));
      i++;
    }
  return (s);
}

int	**convert_tab_char_to_int(char **tabc, int **tabi)
{
  int	x;
  int	y;

  x = 0;
  while (tabc[x])
    {
      y = 0;
      while (tabc[x][y])
	{
	  if (tabc[x][y] == '*')
	    tabi[x][y] = WALL;
	  else if (tabc[x][y] == ' ')
	    tabi[x][y] = SPACE;
	  else if (tabc[x][y] == '1')
	    tabi[x][y] = ENTRY;
	  else if (tabc[x][y] == '2')
	    tabi[x][y] = EXIT;
	  y++;
	}
      x++;
    }
  return (tabi);
}

char	**convert_tab_int_to_char(char **tabc, int **tabi)
{
  int	x;
  int	y;

  x = 0;
  while (tabc[x])
    {
      y = 0;
      while (tabc[x][y])
	{
	  if (tabi[x][y] == WALL)
	    tabc[x][y] = '*';
	  else if (tabi[x][y] == SPACE)
	    tabc[x][y] = ' ';
	  else if (tabi[x][y] == ENTRY)
	    tabc[x][y] = '1';
	  else if (tabi[x][y] == EXIT)
	    tabc[x][y] = '2';
	  else if (tabi[x][y] == WAY)
	    tabc[x][y] = 'o';
	  y++;
	}
      tabc[x][y] = 0;
      x++;
    }
  tabc[x] = NULL;
  return (tabc);
}

int	show_mabite(int **tab, int x1, int y1)
{
  int	x;
  int	y;

  x = 0;
  while (x < x1)
    {
      y = 0;
      while (y < y1)
	{
	  my_put_nbr(tab[x][y]);
	  y++;
	}
      my_putchar('\n');
      x++;
    }
}

char		**find_the_way(char **s)
{
  int	**tab;
  char	**tabc;
  int	i;

  i = 0;
  tabc = malloc(255 * sizeof(*tabc));
  while (i < 124)
    {
      tabc[i] = malloc(64 * sizeof(tabc));
      i++;
    }
  tab = mk_tabint(s);
  tab = convert_tab_char_to_int(s, tab);
  my_putchar('\n');
  show_mabite(tab, my_tablen(s), my_strlen(s[0]));
  my_putchar('\n');
  tabc = convert_tab_int_to_char(s, tab);
  show_tab(tabc);
  my_putchar('\n');
  return (s);
}
