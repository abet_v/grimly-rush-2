##
## Makefile for minishell in /home/abet_v//minishell
## 
## Made by vincent abet
## Login   <abet_v@epitech.net>
## 
## Started on  Tue Dec 11 21:55:27 2012 vincent abet
## Last update Fri Mar  1 23:12:41 2013 vincent abet
##

SRC=	main.c \
	get_next_line.c \
	grimly.c \

OBJ=	$(SRC:.c=.o)

LIB=	-lmy -L. -ltermcap

CC=	gcc

CFLAG=	-pedantic -W -Wall -Wextra -ansi

NAME=	grimly

all:	$(NAME)

$(NAME):	$(OBJ)
	make -C my/
	$(CC) -o $(NAME) $(OBJ) $(LIB) $(CFLAG)

clean:	
	rm -f $(OBJ)

fclean:	clean
	rm -f $(NAME)

re:	fclean all